<%-- 
    Document   : unidadorganizacion
    Created on : 09-jul-2016, 16:54:13
    Author     : deadpc
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.Asignatura"%>
<%@page import="modelo.Perfilprofesores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
    List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Unidad Organizacional</h1>
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-3">Asignaturas</label>
                <div class="col-xs-9">
                    <select class="form-control" id="asignatura">
                        <% for (Asignatura var : asignaturas) {%>
                        <option value="<% out.print(var.getId());  %>"><% out.print(var.getNombreasignatura());  %></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Créditos</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="creditos" placeholder="Créditos">
                </div>
            </div>          
            <div class="form-group">
                <label class="control-label col-xs-3">Correquisitos</label>
                <div class="col-xs-9">
                    <textarea id="correquisitos" rows="3" class="form-control"></textarea>
                </div>
            </div>      
            <div class="form-group">
                <label class="control-label col-xs-3">Asignatura</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="asignatura_1" placeholder="Asignatura">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Nivel</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="nivel" placeholder="Nivel">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Código</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="codigo" placeholder="Código">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var asignatura = "";
                $("select option:selected").each(function () {
                    asignatura = $(this).val().trim();
                });
                var formData = {
                    accion: "save",
                    creditos: $('#creditos').val(),
                    correquisitos: $('#correquisitos').val(),
                    asignatura_1: $('#asignatura_1').val(),
                    nivel: $('#nivel').val(),
                    codigo: $('#codigo').val(),
                    asignaturaid: asignatura
                };

                $.ajax({
                    type: 'POST',
                    url: 'UnidadOrganizacionController',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    console.log(data);
                }).fail(function (data) {
                    console.log(data);
                });
            }


            function limpiar() {
                $('#creditos').val('');
                $('#correquisitos').val('');
                $('#asignatura_1').val('');
                $('#nivel').val('');
                $('#codigo').val('');
            }


        </script>

    </body>
</html>
