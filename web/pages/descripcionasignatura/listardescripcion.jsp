<%-- 
    Document   : ListarAsignaturas
    Created on : 15-jul-2016, 19:24:59
    Author     : deadpc
--%>

<%@page import="modelo.Perfilprofesores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <input id="idProfesor" type="hidden" value="<% out.print(pp.getId());%>">
        <div id="mostrarDescripcion">

        </div>
        <div id="actualizar" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Descripcion de Asignatura </h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-horizontal">                            
                            <div class="form-group">
                                <label class="control-label col-xs-3">Propósito</label>
                                <div class="col-xs-9">
                                    <textarea id="proposito" rows="5" class="form-control"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-xs-3">Descripción</label>
                                <div class="col-xs-9">
                                    <textarea id="descripcion" rows="3" class="form-control"></textarea>
                                </div>
                            </div>      
                            <div class="form-group">
                                <label for="" class="control-label col-xs-3">Objetivo General</label>
                                <div class="col-xs-9">
                                    <textarea id="objetivogeneral" rows="3" class="form-control"></textarea>
                                </div>
                            </div>

                            <br>
<!--                            <div class="form-group">
                                <div class="col-xs-offset-3 col-xs-9">
                                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                                    <input type="reset" class="btn btn-default" value="Limpiar">
                                </div>
                            </div>-->
                        </div>
                        <script>
                            function listar() {
                                var idProfesor = $('#idProfesor').val();
                                $.ajax({
                                    type: 'GET',
                                    url: 'webresources/DescripcionAsignatura/getAllDescripcionAsignatura?id=' + idProfesor,
                                    dataType: 'json'
                                }).done(function (data) {
                                    console.log(JSON.stringify(data));
                                    var str = '';
                                    str += '<table class="table table-condensed table-responsive">';
                                    str += '    <tr>';
                                    str += '        <th>';
                                    str += '            Asignatura';
                                    str += '        </th>';
                                    str += '        <th>';
                                    str += '            Propósito';
                                    str += '        </th>';                                   
                                    str += '        <th>';
                                    str += '        </th>';
                                    str += '    </tr>';
                                    for (i in data) {
                                        str += '    <tr>';
                                        str += '        <td>';
                                        str += data[i].asignatura;
                                        str += '         </td>';
                                        str += '        <td>';
                                        str += data[i].proposito;
                                        str += '         </td>';                                       
                                        str += '        <td>';
                                        str += '           <button class="btn btn-default" onclick="llenardatos(' + data[i].id + ')"><span class="fa fa-pencil-square-o"></span></button>';
                                        str += '           <button class="btn btn-default" onclick="verEliminar(' + data[i].id + ')"><span class="fa fa-trash-o "></span></button>';
                                        str += '           <button class="btn btn-default" onclick="vista(' + data[i].id + ')"><span class="fa fa-eye"></span></button>';
                                        str += '       </td>';
                                        str += '    </tr>';
                                    }
                                    str += '        </table>';
                                    document.getElementById('mostrarDescripcion').innerHTML = str;
                                }).fail(function (res) {
                                    alert('error');
                                });
                            }
                            $(document).ready(function (evt) {
                                listar();
                            })
                        </script>
                        </body>
                        </html>
