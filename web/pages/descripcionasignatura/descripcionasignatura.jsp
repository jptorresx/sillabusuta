<%-- 
    Document   : descripcionasignatura
    Created on : 09-jul-2016, 16:47:00
    Author     : deadpc
--%>

<%@page import="java.util.List"%>
<%@page import="modelo.Asignatura"%>
<%@page import="modelo.Perfilprofesores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
    List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Descripción Asignatura</h1>
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-3">Asignaturas</label>
                <div class="col-xs-9">
                    <select class="form-control" id="asignatura">
                        <% for (Asignatura var : asignaturas) {%>
                        <option value="<% out.print(var.getId());  %>"><% out.print(var.getNombreasignatura());  %></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Propósito</label>
                <div class="col-xs-9">
                    <textarea id="proposito" rows="5" class="form-control"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-3">Descripción</label>
                <div class="col-xs-9">
                    <textarea id="descripcion" rows="3" class="form-control"></textarea>
                </div>
            </div>      
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Objetivo General</label>
                <div class="col-xs-9">
                    <textarea id="objetivogeneral" rows="3" class="form-control"></textarea>
                </div>
            </div>

            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var asignatura = "";
                $("select option:selected").each(function () {
                    asignatura = $(this).val().trim();
                });
                var formData = {
                    accion: "save",
                    proposito: $('#proposito').val(),
                    descripcionasignatura: $('#descripcion').val(),
                    objetivogeneral: $('#objetivogeneral').val(),
                    asignaturaid: asignatura
                };

                $.ajax({
                    type: 'POST',
                    url: 'DescripcionAsignaturaController',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    console.log(data);
                }).fail(function (data) {
                    console.log(data);
                });
            }


            function limpiar() {
                $('#proposito').val('');
                $('#descripcion').val('');
                $('#objetivogeneral').val('');
            }


        </script>
    </body>
</html>
