<%-- 
    Document   : perfilprofesores
    Created on : 09-jul-2016, 14:35:06
    Author     : deadpc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>

        <div class="page-header">
            <h3>Datos Personales</h3>
        </div>

        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-3">Nombre del Docente</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="nombredocente" placeholder="Nombre del Docente">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Titulo de Cuarto Nivel</label>
                <div class="col-xs-9">
                    <textarea id="titulocuartonivel" rows="3" class="form-control" placeholder="Titulo de Cuarto Nivel"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Área de Conocimiento</label>
                <div class="col-xs-9">
                    <input id="areaconocimiento" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Titulo de Tercer Nivel</label>
                <div class="col-xs-9">
                    <textarea id="titulotercernivel" rows="3" class="form-control" placeholder="Titulo de Tercer Nivel"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Experiencia Profesional</label>
                <div class="col-xs-9">
                    <input id="experienciaprofesional" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Experiencia Docente</label>
                <div class="col-xs-9">
                    <input id="experienciadocente" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Área Académica</label>
                <div class="col-xs-9">
                    <input id="areaacademica" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Objetivos específicos</label>
                <div class="col-xs-9">
                    <textarea id="objetivosespecificos" rows="3" class="form-control" placeholder="Objetivos específicos"></textarea>
                </div>
            </div>      
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Teléfonos</label>
                <div class="col-xs-9">
                    <input id="telefonos" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Email</label>
                <div class="col-xs-9">
                    <input id="email" type="email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Usuario</label>
                <div class="col-xs-9">
                    <input id="email" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Clave</label>
                <div class="col-xs-9">
                    <input id="clave" type="password" class="form-control">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar();" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var formData = {
                    accion: 'save',
                    nombredocente: $('#nombredocente').val(),
                    titulocuartonivel: $('#titulocuartonivel').val(),
                    areaconocimiento: $('#areaconocimiento').val(),
                    titulotercernivel: $('#titulotercernivel').val(),
                    experienciaprofesional: $('#experienciaprofesional').val(),
                    experienciadocente: $('#experienciadocente').val(),
                    areaacademica: $('#areaacademica').val(),
                    objetivosespecificos: $('#objetivosespecificos').val(),
                    telefonos: $('#telefonos').val(),
                    email: $('#email').val(),
//                    usuario: $('#usuario:').val(),
                    clave: $('#clave').val()
                }
                $.ajax({
                    type: 'POST',
                    url: 'AsignaturaDaoImplement',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    alert(JSON.stringify(data));
                }).fail(function (data) {
                    console.log(data);
                });
            }
        </script>
    </body>
</html>
