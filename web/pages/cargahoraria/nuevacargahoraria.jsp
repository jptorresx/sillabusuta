<%-- 
    Document   : nuevacargahoraria
    Created on : 09-jul-2016, 16:42:36
    Author     : deadpc
--%>

<%@page import="modelo.Perfilprofesores"%>
<%@page import="java.util.List"%>
<%@page import="modelo.Asignatura"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
    List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Nueva Carga Horaria</h1>
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-3">Asignaturas</label>
                <div class="col-xs-9">
                    <select class="form-control" id="asignatura">
                        <% for (Asignatura var : asignaturas) {%>
                        <option value="<% out.print(var.getId());  %>"><% out.print(var.getNombreasignatura());  %></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas de Clase</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horasclase" placeholder="Horas de Clase">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas de Clase Teórica</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horasclaseteorica" placeholder="Horas de Clase Teórica">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Horas de Clase Práctica</label>
                <div class="col-xs-9">
                    <input id="horasclasepractica" type="text" class="form-control" placeholder="Horas de Clase Práctica">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Horas de Tutoria Presencial</label>
                <div class="col-xs-9">
                    <input id="horastutoriapresencial" type="text" class="form-control"  placeholder="Horas de Tutoria Presencial">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Horas de Tutoria Virtual</label>
                <div class="col-xs-9">
                    <input id="horastutoriavirtual" type="text" class="form-control" placeholder="Horas de Tutoria Virtual">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Tutoria Académica</label>
                <div class="col-xs-9">
                    <input id="tutoriaacademica" type="text" class="form-control" placeholder="Tutoria Académica">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var asignatura="";
                $("select option:selected").each(function () {
                    asignatura = $(this).val().trim();
                });
                var formData = {
                    accion: "save",
                    horasclase: $('#horasclase').val(),
                    horasclaseteorica: $('#horasclaseteorica').val(),
                    horasclasepractica: $('#horasclasepractica').val(),
                    horastutoriapresencial: $('#horastutoriapresencial').val(),
                    horastutoriavirtual: $('#horastutoriavirtual').val(),
                    tutoriaacademica: $('#tutoriaacademica').val(),
                    asignaturaid: asignatura
                };

                $.ajax({
                    type: 'POST',
                    url: 'CargaHorariaController',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    console.log(data);
                }).fail(function (data) {
                    console.log(data);
                });
            }


            function limpiar() {
                $('#horasclase').val('');
                $('#horasclaseteorica').val('');
                $('#horasclasepractica').val('');
                $('#horastutoriapresencial').val('');
                $('#horastutoriavirtual').val('');
                $('#tutoriaacademica').val('');


            }


        </script>
    </body>
</html>
