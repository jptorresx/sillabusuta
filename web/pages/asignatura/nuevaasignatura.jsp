<%-- 
    Document   : nuevaasignatura
    Created on : 09-jul-2016, 15:59:00
    Author     : deadpc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Nueva Asignatura</h1>
        <form class="form-horizontal">
            <div class="form-group">
                <label class="control-label col-xs-3">Nombre de la asignatura</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="nombreasignatura" placeholder="Nombre de la asignatura">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Carrera</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="carrera" placeholder="Carrera">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Periodo:</label>
                <div class="col-xs-3">
                    <select id="periodo" class="form-control">
                        <option value="2014"> </option>
                        <option value="2015">2014-2015</option>
                        <option value="2016">2016</option>
                    </select>
                </div>                
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Código</label>
                <div class="col-xs-9">
                    <input id="codigo" type="text" class="form-control" placeholder="Codigo">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Objetivos Específicos</label>
                <div class="col-xs-9">
                    <input id="objetivosespecificos" type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Prerequisitos</label>
                <div class="col-xs-9">
                    <input id="prerrequisitos" type="text" class="form-control"  placeholder="prerrequisitos">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Modalidad</label>
                <div class="col-xs-9">
                    <input id="modalidad" type="text" class="form-control" placeholder="modalidad">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Asignatura</label>
                <div class="col-xs-9">
                    <input id="asigna" type="text" class="form-control" placeholder="asignatura">
                </div>
            </div>
            <div class="form-group">
                <label for="" class="control-label col-xs-3">Código</label>
                <div class="col-xs-9">
                    <input id="codigo2" type="text" class="form-control" placeholder="codigo">
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var formData = {
                    accion:"save",
                    nombreasignatura: $('#nombreasignatura').val(),
                    carrera: $('#carrera').val(),
                    periodo: $('#periodo').val(),
                    codigo: $('#codigo').val(),
                    objetivosespecificos: $('#objetivosespecificos').val(),
                    prerrequisitos: $('#prerrequisitos').val(),
                    modalidad: $('#modalidad').val(),
                    asigna: $('#asigna').val(),
                    codigo2: $('#codigo2').val(),
                    perfilprofesoresid: $('#perfilprofesoresid').val()
                };

               $.ajax({
                    type: 'POST',
                    url: 'AsignaturaController',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    console.log(data);
                }).fail(function (data) {
                    console.log(data);
                });
            }
            
            
            function limpiar() {
                $('#nombreasignatura').val('');
                $('#carrera').val('');
                $('#codigo').val('');
                $('#objetivosespecificos').val('');
                $('#prerrequisitos').val('');
                $('#modalidad').val('');
                $('#asignatura').val('');
                $('#codigo2').val('');
               
            }


        </script>
    </body>
</html>
