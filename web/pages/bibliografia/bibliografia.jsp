<%-- 
    Document   : unidadorganizacion
    Created on : 09-jul-2016, 16:54:13
    Author     : deadpc
--%>

<%@page import="modelo.Asignatura"%>
<%@page import="java.util.List"%>
<%@page import="modelo.Perfilprofesores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
    List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Nueva Bibliografia</h1>
        <form class="form-horizontal" method="post" action="BibliografiaController" enctype="multipart/form-data">
            <div class="form-group">
                <label class="control-label col-xs-3">Asignaturas</label>
                <div class="col-xs-9">
                    <select class="form-control" name="asignatura">
                        <% for (Asignatura var : asignaturas) {%>
                        <option value="<% out.print(var.getId());  %>"><% out.print(var.getNombreasignatura());  %></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Autores</label>
                <div class="col-xs-9">
                    <input type="hidden" name="accion" value="save">
                    <input type="" class="form-control" name="autores" name="autores" placeholder="Autores">
                </div>
            </div>          
            <div class="form-group">
                <label class="control-label col-xs-3">Año</label>
                <div class="col-xs-9">
                    <input type="number" class="form-control" name="anio" placeholder="Año">
                </div>
            </div>      
            <div class="form-group">
                <label class="control-label col-xs-3">Titulo</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="titulo" placeholder="Titulo">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Numero de Edición</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="numedicion" placeholder="Numero de edición">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Editorial</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="editorial" placeholder="Editorial">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Ciudad</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="ciudad" placeholder="Ciudad">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Numero de Páginas</label>
                <div class="col-xs-9">
                    <input type="number" class="form-control" name="numpaginas" placeholder="Numero de Paginas">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-3">Url de la Bibliografia</label>
                <div class="col-xs-9">
                    <input type="file" data-filename-placement="inside" name="fichero" accept="application/pdf">
                    <!--<input type="file" class="form-control" id="urlbibliografia" placeholder="Numero de Paginas">-->
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Código Físico</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="codigofisico">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Código Digital</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="codigodigital">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-xs-3">Codigo Virtual</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="codigovirtual">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Numero de Ejemplares</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" name="numejemplares">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Comentario</label>
                <div class="col-xs-9">
                    <textarea name="comentario" rows="3" class="form-control"></textarea>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="submit" class="btn btn-primary" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
    </body>
</html>
