<%-- 
    Document   : programaestudios
    Created on : 09-jul-2016, 16:53:58
    Author     : deadpc
--%>

<%@page import="modelo.Asignatura"%>
<%@page import="modelo.Perfilprofesores"%>
<%@page import="java.util.List"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores pp = (Perfilprofesores) session.getAttribute("user");
    List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Programa de Estudios</h1>
        <form class="form-horizontal">
             <div class="form-group">
                <label class="control-label col-xs-3">Asignaturas</label>
                <div class="col-xs-9">
                    <select class="form-control" id="asignatura">
                        <% for (Asignatura var : asignaturas) {%>
                        <option value="<% out.print(var.getId());  %>"><% out.print(var.getNombreasignatura());  %></option>
                        <%}%>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Unidades Temáticas</label>
                <div class="col-xs-9">
                    <textarea id="unidadestematicas" rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas Teóricas</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horasteoricas" placeholder="Horas de Clase Teórica">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas Prácticas</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horaspracticas" placeholder="Horas de Clase Prácticas">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas de Tutoría</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horastutorias" placeholder="Horas de Tutorías">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Horas de Trabajo Autónomo</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="horastrabajoautonomo" placeholder="Horas de Trabajo Autónomo">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Mecanismos de Instrumenos de Evaluación</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="mecanismo" placeholder="Horas de Trabajo Autónomo">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Resultados de Aprendizaje</label>
                <div class="col-xs-9">
                    <textarea id="resultadosaprendizaje" rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Metodología de Aprendizjae</label>
                <div class="col-xs-9">
                    <textarea id="metodologiaaprendizaje" rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Estrategias Educativas</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="estrategiaseducativas" placeholder="Estrategias Educativas">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-xs-3">Recursos Didácticos</label>
                <div class="col-xs-9">
                    <input type="" class="form-control" id="recursosdidacticos" placeholder="Recursos Didácticos">
                </div>
            </div>

            <br>
            <div class="form-group">
                <div class="col-xs-offset-3 col-xs-9">
                    <input type="button" class="btn btn-primary" onclick="guardar()" value="Guardar">
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </div>
            </div>
        </form>
        <script>
            function guardar() {
                var asignatura="";
                $("select option:selected").each(function () {
                    asignatura = $(this).val().trim();
                });
                var formData = {
                    accion: "save",
                    undadestematicas: $('#unidadestematicas').val(),
                    horasteoricas: $('#horasteoricas').val(),
                    horaspracticas: $('#horaspracticas').val(),
                    horastutorias: $('#horastutorias').val(),
                    horastrabajoautonomo: $('#horastrabajoautonomo').val(),
                    mecanismo : $('#mecanismo').val(),
                    resultadosaprendizaje: $('#resultadosaprendizaje').val(),
                    metodologiaaprendizaje: $('#metodologiaaprendizaje').val(),
                    estrategiaseducativas: $('#estrategiaseducativas').val(),
                    recursosdidacticos: $('#recursosdidacticos').val(),             
                    asignaturaid: asignatura
                };

                $.ajax({
                    type: 'POST',
                    url: 'ProgramaEstudiosController',
                    data: formData
//                        dataType: 'text'
                }).done(function (data) {
                    console.log(data);
                }).fail(function (data) {
                    console.log(data);
                });
            }


            function limpiar() {
                $('#unidadestematicas').val('');
                $('#horasteoricas').val('');
                $('#horaspracticas').val('');
                $('#horastutorias').val('');
                $('#horastrabajoautonomo').val('');
                $('#mecanismosinstrumentosevaluacion').val('');
                $('#resultadosaprendizaje').val('');
                $('#metodologiaaprendizaje').val('');
                $('#estrategiaseducativas').val('');
                $('#recursosdidacticos').val('');
            }
             </script>
    </body>
</html>
