<%-- 
    Document   : index
    Created on : Jul 16, 2016, 9:53:59 AM
    Author     : juan_
--%>

<%@page import="modelo.Perfilprofesores"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Perfilprofesores p = new Perfilprofesores();
    
    try {
        if (session != null) {
            p= (Perfilprofesores)session.getAttribute("usuario");
        }else{
            response.sendRedirect("/sillabus/login.html");
        }
    } catch (NullPointerException e) {
        response.sendRedirect("/sillabus/login.html");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Sistema Biblioteca</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- styles -->
        <link href="css/styles.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <script src="libs/jspdf.min.js"></script>
        <script src="libs/jspdf.plugin.autotable.src.js"></script>
        <!--<script src="font-awesome/css/font-awesome.css"></script>-->
    </head>
    <body>
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <!-- Logo -->
                        <div class="logo">
                            <h1><a href="index.html">Sistema Sillabus</a></h1>
                        </div>
                    </div>
                    <div class="col-md-5 text-right">
                        <h6 style="padding-top: 10px; color: white; text-transform: uppercase;">Bienvenido, <% out.print(p.getNombreprofesora());%>  <img src="<% out.print(p.getFoto());%>" height="30" alt="..." class="img-circle"></h6>
                    </div>
                    <div class="col-md-2">
                        <div class="navbar navbar-inverse" role="banner">
                            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mi cuenta<b class="caret"></b></a>
                                        <ul class="dropdown-menu animated fadeInUp">
                                            <!--<li><a href="profile.html">Perfil</a></li>-->
                                            <li>
                                                <a href="SalirController">Salir</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="page-content">
            <div class="row">
                <div class="col-md-2">
                    <div class="sidebar content-box" style="display: block;">
                        <ul class="nav">
                            <!-- Main menu -->
                            <li class="current"><a href="index.jsp"><i class="glyphicon glyphicon-home"></i> Inicio</a></li>
                            <!--                            <li><a href="calendar.html"><i class="glyphicon glyphicon-calendar"></i> Calendar</a></li>
                                                        <li><a href="stats.html"><i class="glyphicon glyphicon-stats"></i> Statistics (Charts)</a></li>
                                                        <li><a href="tables.html"><i class="glyphicon glyphicon-list"></i> Tables</a></li>
                                                        <li><a href="buttons.html"><i class="glyphicon glyphicon-record"></i> Buttons</a></li>
                                                        <li><a href="editors.html"><i class="glyphicon glyphicon-pencil"></i> Editors</a></li>
                                                        <li><a href="forms.html"><i class="glyphicon glyphicon-tasks"></i> Forms</a></li>-->
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i> Asignaturas
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/asignatura/listarasignaturas.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/asignatura/nuevaasignatura.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-check"></i> Unidad de Organización
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/unidadorganizacion/listarorganizacion.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/unidadorganizacion/unidadorganizacion.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i> Descripción Asignatura
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/descripcionasignatura/listardescripcion.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/descripcionasignatura/descripcionasignatura.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-calendar"></i> Carga Horaria
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/cargahoraria/listarcarga.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/cargahoraria/nuevacargahoraria.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-user"></i> Criterios Normativos
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/criteriosnormativos/listarcriterios.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/criteriosnormativos/criteriosnormativos.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i> Programa de Estudios
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/programaestudios/listarprograma.jsp">Listar</a></li>
                                    <li><a id="nav" href="pages/programaestudios/programaestudios.jsp">Nuevo</a></li>
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i> Bibliografía
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a id="nav" href="pages/bibliografia/bibliografia.jspp">Nuevo</a></li>
                                    <!--<li><a id="nav" href="pages/programaestudios/programaestudios.jsp">Nuevo</a></li>-->
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <li class="submenu">
                                <a href="#">
                                    <i class="glyphicon glyphicon-book"></i> Otros
                                    <span class="caret pull-right"></span>
                                </a>
                                <!-- Sub menu -->
                                <ul>
                                    <li><a href="https://mi-curso-ueja.appspot.com/course" target="_blank">Moocs</a></li>
                                    <!--<li><a id="nav" href="pages/programaestudios/programaestudios.jsp">Nuevo</a></li>-->
                                    <!--<li><a href="signup.html">Signup</a></li>-->
                                </ul>
                            </li>
                            <!--https://mi-curso-ueja.appspot.com/course-->
                        </ul>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="content-box-large" id="mostrar">
                            <object type="text/html" data="http://biblioteca.uta.edu.ec/cgi-bin/wxis.exe/iah/scripts/?IsisScript=iah.xis&lang=es&base=BFCHE" width="100%" height="1200"></object> 
                        </div>                     
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">

                <div class="copy text-center">
                    <a href='#'>Universidad Tecnológica de Ambato</a>
                </div>

            </div>
        </footer>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/custom.js"></script>
        <script>
            $(document).ready(function (e) {
                var x;
                x = $(document);
                x.ready(inicio);

                function inicio() {
                    var x;
                    x = $("a#nav");
                    x.click(muestrame);
                }
                function muestrame() {
                    var pagina = $(this).attr("href");
                    var x = $("#mostrar");
                    x.load(pagina);
                    return false;
                }
            });

            function salir() {
                var forData = {
                    accion: "salir"
                };
                $.ajax({
                    type: 'POST',
                    url: 'login',
                    data: forData
//                        dataType: 'text'
                }).done(function (data) {
                }).fail(function (data) {
                    console.log(data);
                });
            }
        </script>
    </body>
</html>
