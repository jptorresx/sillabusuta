/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import estructuras.ProgramaEstudioEst;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import modelo.Asignatura;
import modelo.Perfilprofesores;
import modelo.Programaestudios;

/**
 * REST Web Service
 *
 * @author deadpc
 */
@Path("Programaestudio")
public class ProgramaestudioResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ProgramaestudioResource
     */
    public ProgramaestudioResource() {
    }

    /**
     * Retrieves representation of an instance of ws.ProgramaestudioResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of ProgramaestudioResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @GET
    @Path("/getAllProgramaEstudio")
    @Produces("application/json")
    public String getAllProgramaEstudio(@QueryParam("id") Integer id) {
         Perfilprofesores pp = dao.PerfilProfesoresDaoImplement.getId(id);
         List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
         List<Programaestudios> list = new ArrayList<>();
         for (Asignatura var : asignaturas) {
             list.addAll(dao.ProgramaEstudiosDaoImplement.getAllAsignaturaId(var.getId()));
         }
         List<ProgramaEstudioEst> lista2 = new  ArrayList<>();
         for (Programaestudios u:list){
             ProgramaEstudioEst unidad = new ProgramaEstudioEst();
             unidad.setUndadestematicas(u.getUndadestematicas());
             unidad.setHorasteoricas(u.getHorasteoricas());
             unidad.setHoraspracticas(u.getHoraspracticas());
             unidad.setHorastutoria(u.getHorastutoria());
             unidad.setHorastrabajoautonomo(u.getHorastrabajoautonomo());
             unidad.setMecanismosintrumentosevaluacion(u.getMecanismosintrumentosevaluacion());
             unidad.setResultadosaprendizaje(u.getResultadosaprendizaje());
             unidad.setMetodologiaaprendizaje(u.getMetodologiaaprendizaje());
             unidad.setEstrategiaseducativas(u.getEstrategiaseducativas());
             unidad.setRecursosdidacticos(u.getRecursosdidacticos());
             
             unidad.setAsignatura(u.getAsignatura().getNombreasignatura());
             lista2.add(unidad);
             
         }
         Gson gson = new Gson();
         
        return gson.toJson(lista2);
    }
}
