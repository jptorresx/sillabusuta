/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import modelo.Asignatura;
import modelo.Bibliografia;
import modelo.Perfilprofesores;

/**
 * REST Web Service
 *
 * 
 */
@Path("bibliografia")
public class BibliografiaResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of BibliografiaResource
     */
    public BibliografiaResource() {
    }

    /**
     * Retrieves representation of an instance of ws.BibliografiaResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of BibliografiaResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }

    @GET
    @Path("/getAll")
    @Produces("application/json")
    public String getAll(@QueryParam("id") Integer id) {
        Gson g = new Gson();
        Perfilprofesores p = dao.PerfilProfesoresDaoImplement.getId(1);
        List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getAllAsignaturaId(p.getId());
        List<Bibliografia> bs = new ArrayList();
        for (Asignatura a : asignaturas) {
            bs.addAll(dao.BibliografiaDaoImplement.getAsignatura(a.getId()));
        }
        return g.toJson(bs);
    }
}
