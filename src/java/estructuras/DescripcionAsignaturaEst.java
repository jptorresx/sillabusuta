package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

public class DescripcionAsignaturaEst {
     private Integer id;
     private String asignatura;
     private String proposito;
     private String descripcionasignatura;
     private String objetivogeneral;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

  

    public String getProposito() {
        return proposito;
    }

    public void setProposito(String proposito) {
        this.proposito = proposito;
    }

    public String getDescripcionasignatura() {
        return descripcionasignatura;
    }

    public void setDescripcionasignatura(String descripcionasignatura) {
        this.descripcionasignatura = descripcionasignatura;
    }

    public String getObjetivogeneral() {
        return objetivogeneral;
    }

    public void setObjetivogeneral(String objetivogeneral) {
        this.objetivogeneral = objetivogeneral;
    }
     
     
     
}
