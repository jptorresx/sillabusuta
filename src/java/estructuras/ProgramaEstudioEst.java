package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

public class ProgramaEstudioEst {
    private Integer id;
     private String asignatura;
     private String undadestematicas;
     private Integer horasteoricas;
     private Integer horaspracticas;
     private Integer horastutoria;
     private Integer horastrabajoautonomo;
     private String mecanismosintrumentosevaluacion;
     private String resultadosaprendizaje;
     private String metodologiaaprendizaje;
     private String estrategiaseducativas;
     private String recursosdidacticos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getUndadestematicas() {
        return undadestematicas;
    }

    public void setUndadestematicas(String undadestematicas) {
        this.undadestematicas = undadestematicas;
    }

    public Integer getHorasteoricas() {
        return horasteoricas;
    }

    public void setHorasteoricas(Integer horasteoricas) {
        this.horasteoricas = horasteoricas;
    }

    public Integer getHoraspracticas() {
        return horaspracticas;
    }

    public void setHoraspracticas(Integer horaspracticas) {
        this.horaspracticas = horaspracticas;
    }

    public Integer getHorastutoria() {
        return horastutoria;
    }

    public void setHorastutoria(Integer horastutoria) {
        this.horastutoria = horastutoria;
    }

    public Integer getHorastrabajoautonomo() {
        return horastrabajoautonomo;
    }

    public void setHorastrabajoautonomo(Integer horastrabajoautonomo) {
        this.horastrabajoautonomo = horastrabajoautonomo;
    }

    public String getMecanismosintrumentosevaluacion() {
        return mecanismosintrumentosevaluacion;
    }

    public void setMecanismosintrumentosevaluacion(String mecanismosintrumentosevaluacion) {
        this.mecanismosintrumentosevaluacion = mecanismosintrumentosevaluacion;
    }

    public String getResultadosaprendizaje() {
        return resultadosaprendizaje;
    }

    public void setResultadosaprendizaje(String resultadosaprendizaje) {
        this.resultadosaprendizaje = resultadosaprendizaje;
    }

    public String getMetodologiaaprendizaje() {
        return metodologiaaprendizaje;
    }

    public void setMetodologiaaprendizaje(String metodologiaaprendizaje) {
        this.metodologiaaprendizaje = metodologiaaprendizaje;
    }

    public String getEstrategiaseducativas() {
        return estrategiaseducativas;
    }

    public void setEstrategiaseducativas(String estrategiaseducativas) {
        this.estrategiaseducativas = estrategiaseducativas;
    }

    public String getRecursosdidacticos() {
        return recursosdidacticos;
    }

    public void setRecursosdidacticos(String recursosdidacticos) {
        this.recursosdidacticos = recursosdidacticos;
    }

     
}
