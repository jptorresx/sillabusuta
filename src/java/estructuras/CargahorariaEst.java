package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

import java.util.Set;

public class CargahorariaEst {
    private Integer id;
     private String asignatura;
     private Integer horasclase;
     private Integer horasclaseteorica;
     private Integer horasclasepractica;
     private Integer horastutoriapresencial;
     private Integer horastutoriavirtual;
     private Integer tutoriaacademica;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public Integer getHorasclase() {
        return horasclase;
    }

    public void setHorasclase(Integer horasclase) {
        this.horasclase = horasclase;
    }

    public Integer getHorasclaseteorica() {
        return horasclaseteorica;
    }

    public void setHorasclaseteorica(Integer horasclaseteorica) {
        this.horasclaseteorica = horasclaseteorica;
    }

    public Integer getHorasclasepractica() {
        return horasclasepractica;
    }

    public void setHorasclasepractica(Integer horasclasepractica) {
        this.horasclasepractica = horasclasepractica;
    }

    public Integer getHorastutoriapresencial() {
        return horastutoriapresencial;
    }

    public void setHorastutoriapresencial(Integer horastutoriapresencial) {
        this.horastutoriapresencial = horastutoriapresencial;
    }

    public Integer getHorastutoriavirtual() {
        return horastutoriavirtual;
    }

    public void setHorastutoriavirtual(Integer horastutoriavirtual) {
        this.horastutoriavirtual = horastutoriavirtual;
    }

    public Integer getTutoriaacademica() {
        return tutoriaacademica;
    }

    public void setTutoriaacademica(Integer tutoriaacademica) {
        this.tutoriaacademica = tutoriaacademica;
    }
     
     
}
