/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Perfilprofesores;

/**
 *
 * @author juan_
 */
@WebServlet(name = "PerfilProfesoresController", urlPatterns = {"/PerfilProfesoresController"})
public class PerfilProfesoresController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PerfilProfesoresController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PerfilProfesoresController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accion = request.getParameter("accion");
            if (accion.equals("save")) {
                String nombredocente = request.getParameter("nombredocente");
                String titulocuartonivel = request.getParameter("titulocuartonivel");
                String areaconocimiento = request.getParameter("areaconocimiento");
                String titulotercernivel = request.getParameter("titulotercernivel");
                String experienciaprofesional = request.getParameter("experienciaprofesional");
                String experienciadocente = request.getParameter("experienciadocente");
                String areaacademica = request.getParameter("areaacademica");               
                String telefonos = request.getParameter("telefonos");
                String email = request.getParameter("email");
                String usuario = request.getParameter("usuario");
                String clave = request.getParameter("clave");
                Perfilprofesores p = new Perfilprofesores();
                p.setNombreprofesora(nombredocente);
                p.setTitulocuartonivel(titulocuartonivel);
                p.setAreaconocimiento(areaconocimiento);
                p.setTitulotercernivel(titulotercernivel);
                p.setExperienciaprofesional(experienciaprofesional);
                p.setExperienciadocente(experienciadocente);
                p.setAreaacademica(areaacademica);
                p.setTelefonos(telefonos);
                p.setUsuario(usuario);
                p.setEmail(email);
                p.setClave(clave);
                out.print(dao.PerfilProfesoresDaoImplement.guardar(p));
            } else if (accion.equals("udate")) {
                
            } else if (accion.equals("eliminar")) {
                
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
