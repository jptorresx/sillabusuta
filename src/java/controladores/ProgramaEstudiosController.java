/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Asignatura;
import modelo.Programaestudios;

/**
 *
 * @author deadpc
 */
@WebServlet(name = "ProgramaEstudiosController", urlPatterns = {"/ProgramaEstudiosController"})
public class ProgramaEstudiosController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProgramaEstudiosController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProgramaEstudiosController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accion = request.getParameter("accion");
            if (accion.equals("save")) {
                String asignaturaid = request.getParameter("asignaturaid").trim();
                Asignatura a = dao.AsignaturaDaoImplement.getId(Integer.parseInt(asignaturaid));
                String undadestematicas = request.getParameter("undadestematicas");
                Integer horasteoricas = Integer.parseInt(request.getParameter("horasteoricas"));
                Integer horaspracticas = Integer.parseInt(request.getParameter("horaspracticas"));
                Integer horastutorias = Integer.parseInt(request.getParameter("horastutorias"));
                Integer horastrabajoautonomo = Integer.parseInt(request.getParameter("horastrabajoautonomo"));
                String mecanismosinstrumentosevaluacion = request.getParameter("mecanismo");
                String resultadosaprendizaje = request.getParameter("resultadosaprendizaje");
                String metodologiaaprendizaje = request.getParameter("metodologiaaprendizaje");
                String estrategiaseducativas = request.getParameter("estrategiaseducativas");
                String recursosdidacticos = request.getParameter("recursosdidacticos");
              
                
                Programaestudios c = new Programaestudios();
                c.setUndadestematicas(undadestematicas);
                c.setHorasteoricas(horasteoricas);
                c.setHoraspracticas(horaspracticas);
                c.setHorastutoria(horastutorias);
                c.setHorastrabajoautonomo(horastrabajoautonomo);
                c.setMecanismosintrumentosevaluacion(mecanismosinstrumentosevaluacion);
                c.setResultadosaprendizaje(resultadosaprendizaje);
                c.setMetodologiaaprendizaje(metodologiaaprendizaje);
                c.setEstrategiaseducativas(estrategiaseducativas);
                c.setRecursosdidacticos(recursosdidacticos);
                c.setAsignatura(a);
//                a.setPerfilprofesores(p);
                if (dao.ProgramaEstudiosDaoImplement.guardar(c)) {
                    out.print("True");
                } else {
                    out.print("False");
                }
            } else if (accion.equals("update")) {

            } else if (accion.equals("delete")) {

            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
