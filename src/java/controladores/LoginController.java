/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Perfilprofesores;

/**
 *
 * @author juan_
 */
@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet login</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet login at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String accion = request.getParameter("accion");
            if (accion.equals("entrar")) {
                String urlLogin = "/sillabusuta/login.html";
                String urlAutenticado = "/sillabusuta/index.jsp";
                String user = request.getParameter("usuario");
                String password = request.getParameter("clave");
                try {
                    if (!user.equals("") && !password.equals("")) {
                        Perfilprofesores d = dao.PerfilProfesoresDaoImplement.autentificar(user, password);
                        if (d != null) {
                            if (user.equals(d.getUsuario()) && password.equals(d.getClave())) {
                                session.setAttribute("usuario", d);
//                            sesion.setAttribute("nombres", d.getNombredocente());
//                            sesion.setAttribute("id_user", d.getId());
//                            sesion.setAttribute("tipo_usuario", d.getAreaacademica());
                                response.sendRedirect(urlAutenticado);
                            } else {
                                session.setAttribute("mensaje", "Contraseña incorrecta");
                                response.sendRedirect(urlLogin);
                            }
                        } else {
                            session.setAttribute("mensaje", "Usuario no está registrado. Registrese por favor");
                            response.sendRedirect(urlLogin);
                        }

                    } else {
                        session.setAttribute("mensaje", "Ingrese usuario y contraseña!.");
                        response.sendRedirect(urlLogin);
                    }

                } catch (Exception ex) {
                    response.sendRedirect(urlLogin);
                }
            } else if (accion.equals("salir")) {
                session.invalidate();
                String urlLogin = "/sillabusuta/";
                response.sendRedirect(urlLogin);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
