/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.List;
import modelo.Descripcionasignatura;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DescripcionAsignaturaDaoImplement {

    public static List<Descripcionasignatura> getAllAsignaturaId(Integer id) {
        List<Descripcionasignatura> list = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Descripcionasignatura where asignaturaid=" + id);
            list = q.list();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return list;
    }

    public static Descripcionasignatura getId(Integer id) {
       Descripcionasignatura pp = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Descripcionasignatura where id=" + id);
            pp = (Descripcionasignatura) q.uniqueResult();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return pp;
    }

    public static boolean guardar(Descripcionasignatura pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.save(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean actualizar(Descripcionasignatura pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.update(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean eliminar(Descripcionasignatura pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.delete(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static List<Descripcionasignatura> getAsignaturaid(Integer id) {
        List<Descripcionasignatura> list = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Descripcionasignatura where asignaturaid =" + id);
            list = (List<Descripcionasignatura>) q.list();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return list;
    }

//    public static void main(String[] args) {
//        List<String> list = new ArrayList<>();
//        for (Descripcionasignatura var : getAll(1)) {
//            System.out.println(var.getId());
//        }
//    }
}
