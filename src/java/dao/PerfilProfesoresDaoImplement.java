/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.Perfilprofesores;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author juan_
 */
public class PerfilProfesoresDaoImplement {

    public static List<Perfilprofesores> getAll() {
        List<Perfilprofesores> list = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Perfilprofesores where activo=true");
            list = q.list();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return list;
    }

    public static boolean guardar(Perfilprofesores pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.save(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean actualizar(Perfilprofesores pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.update(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean eliminar(Perfilprofesores pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.delete(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static Perfilprofesores getId(Integer id) {
        Perfilprofesores pp = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Perfilprofesores where id=" + id);
            pp = (Perfilprofesores) q.uniqueResult();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return pp;
    }

    public static Perfilprofesores autentificar(String usuario, String clave) {
        Perfilprofesores pp = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Perfilprofesores where usuario='" + usuario + "' and clave='" + clave + "'");
            pp = (Perfilprofesores) q.uniqueResult();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return pp;
    }

    public static void main(String[] args) {
        Perfilprofesores p = autentificar("admin", "admin");
        System.out.println(p.getAreaacademica());
    }
}
