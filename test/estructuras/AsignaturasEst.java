package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

import java.util.Set;
import modelo.Perfilprofesores;

public class AsignaturasEst {

     private Integer id;
     private Integer perfilprofesoresid;
     private String nombreasignatura;
     private String carrera;
     private String periodo;
     private String codigo;
     private String objetivosespecificos;
     private String prerrequisitos;
     private String modalidad;
     private String asignatura;
     private String codigo2;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPerfilprofesoresid() {
        return perfilprofesoresid;
    }

    public void setPerfilprofesoresid(Integer perfilprofesoresid) {
        this.perfilprofesoresid = perfilprofesoresid;
    }

    public String getNombreasignatura() {
        return nombreasignatura;
    }

    public void setNombreasignatura(String nombreasignatura) {
        this.nombreasignatura = nombreasignatura;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getObjetivosespecificos() {
        return objetivosespecificos;
    }

    public void setObjetivosespecificos(String objetivosespecificos) {
        this.objetivosespecificos = objetivosespecificos;
    }

    public String getPrerrequisitos() {
        return prerrequisitos;
    }

    public void setPrerrequisitos(String prerrequisitos) {
        this.prerrequisitos = prerrequisitos;
    }

    public String getModalidad() {
        return modalidad;
    }

    public void setModalidad(String modalidad) {
        this.modalidad = modalidad;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getCodigo2() {
        return codigo2;
    }

    public void setCodigo2(String codigo2) {
        this.codigo2 = codigo2;
    }
    
    
    
     

   }  
   