package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

import java.util.Set;

public class CriteriosNormativosEst {
      private Integer id;
     private String asignatura;
     private String objtetivosespecificos;
     private String tecnicasEInstrumentos;
     private String evaluaciondiagnostica;
     private String evaluacionformativa;
     private String evaluacionsumativa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    public String getObjtetivosespecificos() {
        return objtetivosespecificos;
    }

    public void setObjtetivosespecificos(String objtetivosespecificos) {
        this.objtetivosespecificos = objtetivosespecificos;
    }

    public String getTecnicasEInstrumentos() {
        return tecnicasEInstrumentos;
    }

    public void setTecnicasEInstrumentos(String tecnicasEInstrumentos) {
        this.tecnicasEInstrumentos = tecnicasEInstrumentos;
    }

    public String getEvaluaciondiagnostica() {
        return evaluaciondiagnostica;
    }

    public void setEvaluaciondiagnostica(String evaluaciondiagnostica) {
        this.evaluaciondiagnostica = evaluaciondiagnostica;
    }

    public String getEvaluacionformativa() {
        return evaluacionformativa;
    }

    public void setEvaluacionformativa(String evaluacionformativa) {
        this.evaluacionformativa = evaluacionformativa;
    }

    public String getEvaluacionsumativa() {
        return evaluacionsumativa;
    }

    public void setEvaluacionsumativa(String evaluacionsumativa) {
        this.evaluacionsumativa = evaluacionsumativa;
    }

     
}
