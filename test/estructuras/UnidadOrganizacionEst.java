package estructuras;
// Generated Jul 2, 2016 2:15:18 PM by Hibernate Tools 4.3.1

import java.util.Set;

public class UnidadOrganizacionEst {
     private Integer id;
     private String asignatura;
     private Integer creditos;
     private String correquisitos;
     private String asignatura_1;
     private String nivel;
     private String codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(String asignatura) {
        this.asignatura = asignatura;
    }

    
    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public String getCorrequisitos() {
        return correquisitos;
    }

    public void setCorrequisitos(String correquisitos) {
        this.correquisitos = correquisitos;
    }

    public String getAsignatura_1() {
        return asignatura_1;
    }

    public void setAsignatura_1(String asignatura_1) {
        this.asignatura_1 = asignatura_1;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
     
     
     
}
