/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import estructuras.UnidadOrganizacionEst;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import modelo.Asignatura;
import modelo.Perfilprofesores;
import modelo.Unidadorganizacion;

/**
 * REST Web Service
 *
 * @author deadpc
 */
@Path("UnidadOrganizacion")
public class UnidadOrganizacionResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UnidadOrganizacionResource
     */
    public UnidadOrganizacionResource() {
    }

    /**
     * Retrieves representation of an instance of ws.UnidadOrganizacionResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of UnidadOrganizacionResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @GET
    @Path("/getAllUnidadOrganizacion")
    @Produces("application/json")
    public String getAllUnidadOrganizacion(@QueryParam("id") Integer id) {
         Perfilprofesores pp = dao.PerfilProfesoresDaoImplement.getId(id);
         List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
         List<Unidadorganizacion> list = new ArrayList<>();
         for (Asignatura var : asignaturas) {
             list.addAll(dao.UnidadOrganizacionDaoImplement.getAllAsignaturaId(var.getId()));
         }
         List<UnidadOrganizacionEst> lista2 = new  ArrayList<>();
         for (Unidadorganizacion u:list){
             UnidadOrganizacionEst unidad = new UnidadOrganizacionEst();
             unidad.setCreditos(u.getCreditos());
             unidad.setCorrequisitos(u.getCorrequisitos());
             unidad.setAsignatura_1(u.getAsignatura_1());
             unidad.setNivel(u.getNivel());
             unidad.setCodigo(u.getCodigo());
             unidad.setAsignatura(u.getAsignatura().getNombreasignatura());
             lista2.add(unidad);
             
         }
         Gson gson = new Gson();
         
        return gson.toJson(lista2);
    }
}
