/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import estructuras.CriteriosNormativosEst;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import modelo.Asignatura;
import modelo.Criteriosnormativos;
import modelo.Perfilprofesores;

/**
 * REST Web Service
 *
 * @author deadpc
 */
@Path("CriteriosNormativos")
public class CriteriosNormativosResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CriteriosNormativosResource
     */
    public CriteriosNormativosResource() {
    }

    /**
     * Retrieves representation of an instance of ws.CriteriosNormativosResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of CriteriosNormativosResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @GET
    @Path("/getAllCriteriosNormativos")
    @Produces("application/json")
    public String getAllCriteriosNormativos(@QueryParam("id") Integer id) {
         Perfilprofesores pp = dao.PerfilProfesoresDaoImplement.getId(id);
         List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
         List<Criteriosnormativos> list = new ArrayList<>();
         for (Asignatura var : asignaturas) {
             list.addAll(dao.CriteriosNormativosDaoImplement.getAllAsignaturaId(var.getId()));
         }
         List<CriteriosNormativosEst> lista2 = new  ArrayList<>();
         for (Criteriosnormativos u:list){
             CriteriosNormativosEst unidad = new CriteriosNormativosEst();
             unidad.setObjtetivosespecificos(u.getObjtetivosespecificos());
             unidad.setTecnicasEInstrumentos(u.getTecnicasEInstrumentos());
             unidad.setEvaluaciondiagnostica(u.getEvaluaciondiagnostica());
             unidad.setEvaluacionformativa(u.getEvaluacionformativa());
             unidad.setEvaluacionsumativa(u.getEvaluacionsumativa());
             
             unidad.setAsignatura(u.getAsignatura().getNombreasignatura());
             lista2.add(unidad);
             
         }
         Gson gson = new Gson();
         
        return gson.toJson(lista2);
    }
}
