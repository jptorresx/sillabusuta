/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import com.google.gson.Gson;
import estructuras.CargahorariaEst;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import modelo.Asignatura;
import modelo.Cargahoraria;
import modelo.Perfilprofesores;

/**
 * REST Web Service
 *
 * @author deadpc
 */
@Path("Cargahoraria")
public class CargahorariaResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of CargahorariaResource
     */
    public CargahorariaResource() {
    }

    /**
     * Retrieves representation of an instance of ws.CargahorariaResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of CargahorariaResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_XML)
    public void putXml(String content) {
    }
    
    @GET
    @Path("/getAllCargaHoraria")
    @Produces("application/json")
    public String getAllCargaHoraria(@QueryParam("id") Integer id) {
         Perfilprofesores pp = dao.PerfilProfesoresDaoImplement.getId(id);
         List<Asignatura> asignaturas = dao.AsignaturaDaoImplement.getPerfilProfesoresId(pp.getId());
         List<Cargahoraria> list = new ArrayList<>();
         for (Asignatura var : asignaturas) {
             list.addAll(dao.CargaHorariaDaoImplement.getAllAsignaturaId(var.getId()));
         }
         List<CargahorariaEst> lista2 = new  ArrayList<>();
         for (Cargahoraria u:list){
             CargahorariaEst unidad = new CargahorariaEst();
             unidad.setHorasclase(u.getHorasclase());
             unidad.setHorasclaseteorica(u.getHorasclaseteorica());
             unidad.setHorasclasepractica(u.getHorasclasepractica());
             unidad.setHorastutoriapresencial(u.getHorastutoriapresencial());
             unidad.setHorastutoriavirtual(u.getHorastutoriavirtual());
             unidad.setTutoriaacademica(u.getTutoriaacademica());
             unidad.setAsignatura(u.getAsignatura().getNombreasignatura());
             lista2.add(unidad);
             
         }
         Gson gson = new Gson();
         
        return gson.toJson(lista2);
    }
}
