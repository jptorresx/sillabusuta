/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author juan_
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(ws.AsignaturaResource.class);
        resources.add(ws.BibliografiaResource.class);
        resources.add(ws.CargahorariaResource.class);
        resources.add(ws.CriteriosNormativosResource.class);
        resources.add(ws.DescripcionAsignaturaResource.class);
        resources.add(ws.PerfilprofesoresResource.class);
        resources.add(ws.ProgramaestudioResource.class);
        resources.add(ws.UnidadOrganizacionResource.class);
    }
    
}
