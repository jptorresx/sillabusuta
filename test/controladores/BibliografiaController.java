/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Asignatura;
import modelo.Bibliografia;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author juan_
 */
@WebServlet(name = "BibliografiaController", urlPatterns = {"/BibliografiaController"})
public class BibliografiaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BibliografiaController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BibliografiaController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String urlFile = "";
            try {
                FileItemFactory factory = new DiskFileItemFactory();
                ServletFileUpload upload = new ServletFileUpload(factory);

// req es la HttpServletRequest que recibimos del formulario.
// Los items obtenidos serán cada uno de los campos del formulario,
// tanto campos normales como ficheros subidos.
                List items = upload.parseRequest(request);
                Bibliografia b = new Bibliografia();
// Se recorren todos los items, que son de tipo FileItem
                for (Object item : items) {
                    FileItem uploaded = (FileItem) item;

                    // Hay que comprobar si es un campo de formulario. Si no lo es, se guarda el fichero
                    // subido donde nos interese
                    if (!uploaded.isFormField()) {
                        // No es campo de formulario, guardamos el fichero en algún sitio
                        File fichero = new File(recursos.VarGlobales.dirFiles(), uploaded.getName());
                        uploaded.write(fichero);
                        urlFile = recursos.VarGlobales.dirFiles() + uploaded.getName();
                        System.out.println(urlFile);
                        b.setUrlbibliografia(urlFile);
                    } else {
                        // es un campo de formulario, podemos obtener clave y valor
                        String key = uploaded.getFieldName();
                        String valor = uploaded.getString();

                        switch (key) {
                            case "autores":
                                b.setAutores(valor);
                                break;
                            case "anio":
                                b.setAnio(Integer.parseInt(valor));
                                break;
                            case "titulo":
                                b.setTitulo(valor);
                                break;
                            case "numedicion":
                                b.setNumedicion(valor);
                                break;
                            case "editorial":
                                b.setEditorial(valor);
                                break;
                            case "ciudad":
                                b.setCiudadpais(valor);
                                break;
                            case "numpaginas":
                                b.setNumpaginas(Integer.parseInt(valor));
                                break;
                            case "codigofisico":
                                b.setCodigofisico(valor);
                                break;
                            case "codigodigital":
                                b.setCodigodigital(valor);
                                break;
                            case "codigovirtual":
                                b.setCodigovirtual(valor);
                                break;
                            case "numejemplares":
                                b.setNumejemplares(valor);
                                break;
                            case "comentario":
                                b.setComentario(valor);
                                break;
                            case "asignatura":
                                Asignatura a = dao.AsignaturaDaoImplement.getId(Integer.parseInt(valor));
                                b.setAsignatura(a);
                                break;

                        }
//                        System.out.println(key + "\t" + valor);

                    }
                }
//                out.print(dao.);
                if (dao.BibliografiaDaoImplement.guardar(b)) {
                    response.sendRedirect("/sillabus/index.jsp");
                }
                out.print(dao.BibliografiaDaoImplement.guardar(b));
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

//            String accion = request.getParameter("accion");
//            if (accion.equals("save")) {
//                HttpSession session = request.getSession();
//                session.getAttribute("user");
//                Perfilprofesores p = (Perfilprofesores) session.getAttribute("user");
//
//            }
        }
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
