/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controladores;

import com.google.gson.Gson;
import estructuras.AsignaturasEst;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import modelo.Asignatura;
import modelo.Perfilprofesores;

/**
 *
 * @author juan_
 */
@WebServlet(name = "AsignaturaController", urlPatterns = {"/AsignaturaController"})
public class AsignaturaController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AsignaturaDaoImplement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AsignaturaDaoImplement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String accion = request.getParameter("accion");
            if (accion.equals("save")) {
                HttpSession session = request.getSession();
                Perfilprofesores p = (Perfilprofesores) session.getAttribute("user");
                String nombreasignatura = request.getParameter("nombreasignatura");
                String carrera = request.getParameter("carrera");
                String periodo = request.getParameter("periodo");
                String codigo = request.getParameter("codigo");
                String objetivosespecificos = request.getParameter("objetivosespecificos");
                String prerrequisitos = request.getParameter("prerrequisitos");
                String modalidad = request.getParameter("modalidad");
                String asignatura = request.getParameter("asigna");

                String codigo2 = request.getParameter("codigo2");
                Asignatura a = new Asignatura();
                a.setNombreasignatura(nombreasignatura);
                a.setCarrera(carrera);
                a.setPeriodo(periodo);
                a.setCodigo(codigo);
                a.setObjetivosespecificos(objetivosespecificos);
                a.setPrerrequisitos(prerrequisitos);
                a.setModalidad(modalidad);
                a.setAsignatura(asignatura);
                a.setCodigo2(codigo2);
                a.setPerfilprofesores(p);
                if (dao.AsignaturaDaoImplement.guardar(a)) {
                    out.print("True");
                } else {
                    out.print("False");
                }
            } else if (accion.equals("update")) {
                Integer id = Integer.parseInt(request.getParameter("id"));
                Asignatura a = dao.AsignaturaDaoImplement.getId(id);
                String nombreasignatura = request.getParameter("nombreasignatura");
                String carrera = request.getParameter("carrera");
                String periodo = request.getParameter("periodo");
                String codigo = request.getParameter("codigo");
                String objetivosespecificos = request.getParameter("objetivosespecificos");
                String prerrequisitos = request.getParameter("prerrequisitos");
                String modalidad = request.getParameter("modalidad");
                String asignatura = request.getParameter("asigna");

                a.setId(id);
                a.setNombreasignatura(nombreasignatura);
                a.setCarrera(carrera);
                a.setPeriodo(periodo);
                a.setCodigo(codigo);
                a.setObjetivosespecificos(objetivosespecificos);
                a.setPrerrequisitos(prerrequisitos);
                a.setModalidad(modalidad);
                a.setAsignatura(asignatura);
                boolean res = dao.AsignaturaDaoImplement.actualizar(a);
                out.print(res);

            } else if (accion.equals("getdatos")) {
                Integer id = Integer.parseInt(request.getParameter("id"));
                Asignatura a = dao.AsignaturaDaoImplement.getId(id);
                String nombreasignatura = request.getParameter("nombreasignatura");
                String carrera = request.getParameter("carrera");
                String periodo = request.getParameter("periodo");
                String codigo = request.getParameter("codigo");
                String objetivosespecificos = request.getParameter("objetivosespecificos");
                String prerrequisitos = request.getParameter("prerrequisitos");
                String modalidad = request.getParameter("modalidad");
                String asignatura = request.getParameter("asigna");
                String codigo2 = request.getParameter("codigo2");
                AsignaturasEst a2 = new AsignaturasEst();

                a2.setId(a.getId());
                a2.setNombreasignatura(a.getNombreasignatura());
                a2.setCarrera(a.getCarrera());
                a2.setPeriodo(a.getPeriodo());
                a2.setCodigo(a.getCodigo());
                a2.setObjetivosespecificos(a.getObjetivosespecificos());
                a2.setPrerrequisitos(a.getPrerrequisitos());
                a2.setModalidad(a.getModalidad());
                a2.setAsignatura(a.getAsignatura());
                a2.setCodigo2(a.getCodigo2());
                Gson gson = new Gson();
                out.println(gson.toJson(a2));

            } else if (accion.equals("delete")) {

            }
        }
//        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
