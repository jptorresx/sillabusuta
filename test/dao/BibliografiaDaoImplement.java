/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import modelo.Bibliografia;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author juan_
 */
public class BibliografiaDaoImplement {

    public static List<Bibliografia> getAll(Integer id) {
        List<Bibliografia> list = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Bibliografia where perfilprofesoresid=" + id);
            list = q.list();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return list;
    }

    public static Bibliografia getId(Integer id) {
        Bibliografia pp = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Bibliografia where id=" + id);
            pp = (Bibliografia) q.uniqueResult();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return pp;
    }

    public static boolean guardar(Bibliografia pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.save(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean actualizar(Bibliografia pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.update(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static boolean eliminar(Bibliografia pp) {
        Transaction t = null;
        Session s = null;
        boolean res = false;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            t = s.beginTransaction();
            s.delete(pp);
            t.commit();
            res = true;
        } catch (HibernateException e) {
            if (t != null) {
                t.rollback();
            }
            res = false;
            System.out.println(e.getMessage());
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return res;
    }

    public static List<Bibliografia> getAsignatura(Integer id) {
        List<Bibliografia> list = null;
        Session s = null;
        try {
            s = HibernateUtil.getSessionFactory().openSession();
            Query q = s.createQuery("from Bibliografia where asignaturaid =" + id);
            list = (List<Bibliografia>) q.list();
        } catch (HibernateException e) {
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return list;
    }
    public static void main(String[] args) {
        for (Bibliografia var : getAsignatura(7)) {
            System.out.println(var.getId());
        }
    }
}
